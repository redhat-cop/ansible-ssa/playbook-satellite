# README

The playbooks in this repository can be used to create, install and configure a Red Hat Satellite Server.

These playbook are using roles and Ansible collections, which are automatically installed if used with Ansible Tower. For manual installation use:

```bash
ansible-galaxy collections install -r collections/requirements.yml
ansible-galaxy install -r roles/requirements.yml
```

Add `-f` to force installation of the latest version of the collections and the roles.

## Create instance

Create a new Virtual Machine or Cloud Instance to install Red Hat Satellite.

```bash
ansible-playbook satellite-instance.yml
```

## Register

To be able to install the Satellite Server and to synchronize channels, the Satellite Server has to be registered.

```bash
ansible-playbook satellite-register.yml
```

## Setup

Install the Satellite Server.

```bash
ansible-playbook satellite-setup.yml
```

## Configure

Configure Satellite content and synchronize RHEL channels.

```bash
ansible-playbook satellite-content.yml
```

Use the following booleans to enable additional content:

- satellite_sync_rhel7: will synchronize RHEL 7 base channels, create a content view and a `RHEL7` activation key

- satellite_sync_rhel8: will synchronize RHEL 8 Base OS and Appstream, create a content view and a `RHEL8` activation key
